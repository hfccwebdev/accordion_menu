$(document).ready(function(){

    $('body > div:nth-child(2)').addClass('sidenav');
    $('body > div:nth-child(2)').attr('id', 'sideaccord');

    $('#sideaccord > ul').addClass('accordion');
    $('#sideaccord > ul').attr('id', 'mainaccordmenu');

    // add parent class and toggle class
    $('#mainaccordmenu li ul').parent().addClass('parent');

    showMenu();

});

function showMenu() {
    $('.toggle').click(function() {

    // close menu item, set left arrowz
        if ($(this).next('ul').hasClass('show')) {
            $(this).removeClass('arrowdown');
            $(this).attr("aria-selected","false");
            $(this).attr("aria-expanded","false");
            $(this).addClass('arrowleft');
            $(this).next('ul').removeClass('show');
            //$(this).next('ul').slideUp(350);
            $(this).parent().parent().removeClass('open');
        }

    // open menu item
        else {
            $(this).parent().parent().find('.parent .toggle').removeClass('arrowdown');
            $(this).parent().parent().find('.parent .toggle').addClass('arrowleft');
            $(this).removeClass('arrowleft');
            $(this).addClass('arrowdown');
            $(this).attr("aria-expanded","true");
            $(this).attr("aria-selected","true");
            $(this).parent().parent().find('.parent ul').removeClass('show');
            //$(this).parent().parent().find('.parent ul').slideUp(350);
            $(this).next('ul').toggleClass('show');
            //$(this).next('ul').slideToggle(350);
            $(this).parent().parent().addClass('open');
        }

    }); // end toggle.click


    $('#mainaccordmenu-open').click(function() {

            if ($('#mainaccordmenu > .parent > ul').hasClass('show')) {
                $(this).removeClass('arrowdown');
                $(this).addClass('arrowleft');
                $(this).attr("aria-expanded","false");
                $(this).attr("aria-selected","false");
                $('#mainaccordmenu > .parent > ul').toggleClass('show');
            }

            else {
                $(this).removeClass('arrowleft');
                $(this).addClass('arrowdown');
                $(this).attr("aria-expanded","true");
                $(this).attr("aria-selected","true");
                $('#mainaccordmenu > .parent > ul').toggleClass('show');
            }

    }); // end mainaccordmenu-open.click

}; // end showMenu function


// when one of the parent list items is toggled to reveal show go back up tree and toggle all of its siblings to be show
